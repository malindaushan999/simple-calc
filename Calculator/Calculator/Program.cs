﻿namespace Calculator
{
    internal class Program
    {
        static void Main(string[] args)
        {
			try
			{
				Calculator calculator = new Calculator();
				int choice = calculator.EnterYourChoice();
				int num1 = calculator.EnterNumberOne();
				int num2 = calculator.EnterNumberTwo();
				calculator.Calculate(num1, num2, choice);
			}
			catch (FormatException ex)
			{
				Console.WriteLine("[ERROR] Please enter valid numbers");
			}
			catch (Exception ex)
			{
				Console.WriteLine("[ERROR] {0}", ex.Message);
			}

			Console.ReadKey();
        }
    }
}