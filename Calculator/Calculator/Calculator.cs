﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class Calculator
    {
        public Calculator()
        {
            Console.WriteLine("---------");
            Console.WriteLine("Functions");
            Console.WriteLine("---------");
            Console.WriteLine("1. Add");
            Console.WriteLine("2. Sub");
            Console.WriteLine("3. Mul");
            Console.WriteLine("4. Div");
        }

        public int EnterYourChoice()
        {
            Console.Write("Enter your choice: ");
            var input = Console.ReadLine();
            return int.Parse(input);
        }

        public int EnterNumberOne()
        {
            Console.Write("Enter num 1: ");
            var input = Console.ReadLine();
            return int.Parse(input);
        }

        public int EnterNumberTwo()
        {
            Console.Write("Enter num 2: ");
            var input = Console.ReadLine();
            return int.Parse(input);
        }

        public void Calculate(double num1, double num2, int choice)
        {
            double result = 0;
            if (choice == 1)
            {
                result = num1 + num2;
            }
            else if (choice == 2)
            {
                result = num1 - num2;
            }
            else if(choice == 3)
            {
                result = num1 * num2;
            }
            else if (choice == 4)
            {
                result = num1 / num2;
            }

            Console.WriteLine("Result = {0}", result);
        }
    }
}
